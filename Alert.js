/*
 * Alert.js
 * app/components
 *
 * Created by Li Theen Kok on 10 May 2017.
 * Copyright (C) 2017 Apptivity Lab. All Rights Reserved.
 */

import React, { Component } from "react"
import PropTypes from "prop-types"
import { Title1, Title2, Title3, Headline, Body, Callout, Subhead, Footnote, Caption1, Caption2 } from "../components/typography"

class Alert extends Component {
    render() {
        const title = this.props.title ? this.props.title.trim() : ""
        let titleElement = ""
        if (title.length > 0) {
            titleElement = <strong>{title}: </strong>
        }

        return (
            <div className="row">
                <div className="col-12 mt-3">
                    <div className={`alert ${"alert-" + this.props.alertType} ${this.props.onDismiss ? "alert-dismissible fade show d-flex justify-content-between" : null}`} role="alert">
                        <div>
                            {titleElement && <Subhead>{titleElement}</Subhead>}
                            {this.props.message}
                            {this.props.children}
                        </div>
                        {
                            this.props.onDismiss &&
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"
                                onClick={() => this.props.onDismiss()}>
                                <span aria-hidden="true">&times;</span>
                            </button>
                        }
                    </div>
                </div>
            </div>
        )
    }
}

Alert.propTypes = {
    "alertType": PropTypes.oneOf(["info", "success", "warning", "danger"]).isRequired,
    "title": PropTypes.string,
    "message": PropTypes.string,
    "children": PropTypes.any,
    "onDismiss": PropTypes.func
}

export default Alert
