import React, { Component } from "react"
import PropTypes from "prop-types"

class ButtonGroup extends Component {
    render() {
        return (
            <div className="btn-toolbar" role="toolbar" aria-label="Toolbar with button groups">
                <div className="btn-group" role="group" aria-label={this.props.label}>
                    {
                        this.props.buttons.map((button, index) => {
                            return (
                                <button key={index}
                                    type="button"
                                    className={this.props.selectedButton === button ? "btn btn-secondary px-4" : "btn btn-outline-secondary px-4"}
                                    onClick={() => this.props.onButtonClick(button)}>{button}</button>
                            )
                        })
                    }
                </div>
            </div>
        )
    }
}

ButtonGroup.propTypes = {
    onButtonClick: PropTypes.func.isRequired,
    buttons: PropTypes.arrayOf(String).isRequired,
    selectedButton: PropTypes.string
}

export default ButtonGroup
