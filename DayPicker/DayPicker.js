import React, { Component } from "react"
import PropTypes from "prop-types"
import DayView from "./DayView"
import "./DayPicker.css"

class DayPicker extends Component {
    constructor(props) {
        super(props)
        this.state = {
            "days": (props.days && typeof props.days === "string") ? props.days.split(",") : []
        }
    }

    onClick(day) {
        !this.props.readOnly && (!this.state.days.includes(day)
            ? this.setState({
                "days": [...this.state.days, day]
            })
            : this.setState({
                "days": this.state.days.filter((d) => d !== day)
            }))
    }

    shouldComponentUpdate(nextProps, nextState) {
        return nextState.days !== this.state.days || this.props.days !== nextProps.days
    }

    componentDidUpdate() {
        this.props.onChange && this.props.onChange(this.getSelectedDays())
    }

    componentWillUpdate(nextProps) {
        if (this.props.days !== nextProps.days) {
            this.setState({
                "days": (nextProps.days && typeof nextProps.days === "string") ? nextProps.days.split(",") : []
            })
        }
    }

    getSelectedDays() {
        return this.state.days.join(",")
    }

    isSelected(day) {
        return this.state.days.includes(day)
    }

    render() {
        const { selectedColor, deselectedColor } = this.props
        return (
            <div>
                <DayView label="Mo" value="mon" handleClick={this.onClick.bind(this)} isSelected={this.isSelected.bind(this)} selectedColor={selectedColor} deselectedColor={deselectedColor} />
                <DayView label="Tu" value="tue" handleClick={this.onClick.bind(this)} isSelected={this.isSelected.bind(this)} selectedColor={selectedColor} deselectedColor={deselectedColor} />
                <DayView label="We" value="wed" handleClick={this.onClick.bind(this)} isSelected={this.isSelected.bind(this)} selectedColor={selectedColor} deselectedColor={deselectedColor} />
                <DayView label="Th" value="thu" handleClick={this.onClick.bind(this)} isSelected={this.isSelected.bind(this)} selectedColor={selectedColor} deselectedColor={deselectedColor} />
                <DayView label="Fr" value="fri" handleClick={this.onClick.bind(this)} isSelected={this.isSelected.bind(this)} selectedColor={selectedColor} deselectedColor={deselectedColor} />
                <DayView label="Sa" value="sat" handleClick={this.onClick.bind(this)} isSelected={this.isSelected.bind(this)} selectedColor={selectedColor} deselectedColor={deselectedColor} />
                <DayView label="Su" value="sun" handleClick={this.onClick.bind(this)} isSelected={this.isSelected.bind(this)} selectedColor={selectedColor} deselectedColor={deselectedColor} />
            </div>
        )
    }
}

DayPicker.propTypes = {
    "days": PropTypes.string,
    "onChange": PropTypes.func,
    "readOnly": PropTypes.bool,
    "selectedColor": PropTypes.string,
    "deselectedColor": PropTypes.string
}

export default DayPicker
