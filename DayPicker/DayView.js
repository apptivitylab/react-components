import React, { Component } from "react"
import PropTypes from "prop-types"

const DayView = ({ label, value, handleClick, isSelected, selectedColor = "black", deselectedColor = "lightgray" }) => (
    <span
        onClick={() => handleClick(value)}
        style={{
            "color": isSelected(value) ? selectedColor : deselectedColor
        }}
    >{label}</span>
)

DayView.propTypes = {
    "label": PropTypes.string.isRequired,
    "value": PropTypes.string.isRequired,
    "handleClick": PropTypes.func.isRequired,
    "isSelected": PropTypes.func.isRequired,
    "selectedColor": PropTypes.string,
    "deselectedColor": PropTypes.string
}

export default DayView
