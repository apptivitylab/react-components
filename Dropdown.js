import React from "react"
import PropTypes from "prop-types"

const Dropdown = {}

Dropdown.Menu = ({
    title = "-",
    key = Math.random().toString(36).substring(7),
    onSelect,
    buttonClassName,
    buttonStyle,
    dropdownMenuClassName,
    children,
    onClick
}) => (
    <div className="dropdown btn-group">
        <button className={buttonClassName || "btn"} style={buttonStyle} type="button" id={key} data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" onClick={onClick}>
            {title}
        </button>
        <div className={dropdownMenuClassName || "dropdown-menu"} aria-labelledby={key}>
            {
                React.Children.map(children.filter((child) => child), child => React.cloneElement(child, { onSelect }))
            }
        </div>
    </div>
)

Dropdown.Item = ({
    title = "-",
    value,
    onSelect = (e) => {},
    children
}) => (
    <a className="dropdown-item" href="javascript:void(0)" onClick={(event) => onSelect && onSelect(event)}>{children}</a>
)

Dropdown.Menu.propTypes = {
    "title": PropTypes.string,
    "key": PropTypes.string,
    "onSelect": PropTypes.function,
    "children": PropTypes.element,
    "buttonClassName": PropTypes.string,
    "buttonStyle": PropTypes.string,
    "dropdownMenuClassName": PropTypes.string,
    "onClick": PropTypes.function
}

Dropdown.Item.propTypes = {
    "value": PropTypes.string,
    "onSelect": PropTypes.function
}

export default Dropdown
