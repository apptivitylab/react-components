/*
 * FileRender.js
 * src/components
 *
 * Created by Li Sim on 16 Aug 2017.
 * Copyright (C) 2017 Apptivity Lab. All Rights Reserved.
 */

import React, { Component } from "react"
import PropTypes from "prop-types"
import { Caption1 } from "./typography"

class FileRender extends Component {
    constructor() {
        super()
        this.defaultStyle = { "maxHeight": "320px", "maxWidth": "320px", "objectFit": "cover" }
    }

    render() {
        return (
            this.props.file
                ? <div>
                    {(() => {
                        switch (this.props.type) {
                        case "Video":
                            const videoAttributes = this.props.file.url
                                ? {
                                    "src": this.props.file.url ? this.props.file.url : this.props.file.uuid,
                                    "poster": this.props.file.preview
                                }
                                : {
                                    "src": this.props.file.preview ? this.props.file.preview : this.props.file.uuid
                                }
                            return (
                                <div className="d-flex flex-column">
                                    <video
                                        {...videoAttributes}
                                        style={this.props.style ? this.props.style : this.defaultStyle}
                                        preload="metadata"
                                        controls
                                    />
                                    {this.props.showFileInfo && this.props.file.name ? this.props.file.name : ""}
                                    {this.props.showFileInfo && this.props.file.size ? <Caption1>{this.props.file.size} bytes</Caption1> : null}
                                </div>
                            )
                        case "Image":
                            let imageUrl = this.props.file.preview ? this.props.file.preview : this.props.file.uuid
                            if (this.props.file.url) { imageUrl = this.props.file.url }
                            return (
                                <div className="d-flex flex-column">
                                    <img style={this.props.style ? this.props.style : this.defaultStyle} src={imageUrl}/>
                                    {this.props.showFileInfo && this.props.file.name ? this.props.file.name : ""}
                                    {this.props.showFileInfo && this.props.file.size ? <Caption1>{this.props.file.size} bytes</Caption1> : null}
                                </div>
                            )
                        default:
                            return (
                                <div className="d-flex flex-column">
                                    <a href={this.props.file.preview ? this.props.file.preview : this.props.file.uuid}>
                                        <i className="material-icons">picture_as_pdf</i> <span>{this.props.file.name ? this.props.file.name : this.props.file.preview}</span>
                                    </a>
                                    {this.props.file.size ? <Caption1>{this.props.file.size} bytes</Caption1> : null}
                                </div>
                            )
                        }
                    })()}
                </div>
                : null
        )
    }
}

FileRender.defaultProps = {
    "showFileInfo": true
}

FileRender.propTypes = {
    "type": PropTypes.string,
    "file": PropTypes.object,
    "style": PropTypes.object,
    "showFileInfo": PropTypes.bool
}

export default FileRender
