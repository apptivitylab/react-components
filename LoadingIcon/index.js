import React, { Component } from "react"
import PropTypes from "prop-types"
import "./index.css"

const LoadingIcon = ({
    loading,
    iconName = "autorenew", // from https://material.io/icons/
    component
}) => component ? <span className="material-icons-spin">{component}</span> : <i className="material-icons material-icons-spin" style={{ "visibility": loading ? "visible" : "hidden" }}>{iconName}</i>

LoadingIcon.propTypes = {
    "loading": PropTypes.bool.isRequired,
    "iconName": PropTypes.string,
    "component": PropTypes.func
}

export default LoadingIcon
