/*
 * NetworkAlert.js
 * app/components
 *
 * Created by Jason Khong on 12th Aug 2017
 * Copyright (C) 2017 Apptivity Lab. All Rights Reserved.
 */

import React, { Component } from "react"
import PropTypes from "prop-types"
import ModalDialog from "./ModalDialog"

class ModalButton extends Component {
    render() {
        return (
            <button type="button"
                className={`btn ${this.props.className || "btn-primary"}`}
                data-toggle="modal"
                disabled={this.props.disabled}
                onClick={(event) => this.props.onModalWillAppear ? this.props.onModalWillAppear(event) : null}
                data-target={`#${this.props.targetId}`}>
                {this.props.title || this.props.children}
            </button>
        )
    }
}

ModalButton.propTypes = {
    "targetId": PropTypes.string,
    "className": PropTypes.string,
    "title": PropTypes.string,
    "fullTitle": PropTypes.string,
    "details": PropTypes.string,
    "onModalWillAppear": PropTypes.func,
    "disabled": PropTypes.bool
}

export default ModalButton
