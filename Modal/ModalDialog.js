/*
 * NetworkAlert.js
 * app/components
 *
 * Created by Jason Khong on 12th Aug 2017
 * Copyright (C) 2017 Apptivity Lab. All Rights Reserved.
 */

import React, { Component } from "react"
import PropTypes from "prop-types"
import uuid from "uuid/v4"

const MODAL_ON_SHOW_EVENT = "show.bs.modal"
const MODAL_ON_SHOWN_EVENT = "shown.bs.modal"
const MODAL_ON_HIDE_EVENT = "hide.bs.modal"
const MODAL_ON_HIDDEN_EVENT = "hidden.bs.modal"

class ModalDialog extends Component {
    constructor(props) {
        super(props)
        this.state = {}
        this.id = props.id || uuid()
        this.$modal = $(`#${this.id}`)
    }

    componentDidMount() {
        if (this.props.show) {
            $(`#${this.id}`).modal("show")
        }
    }

    toggle() {
        $(`#${this.id}`).modal("toggle")
    }

    subscribeLifeCycle() {
        $(`#${this.id}`).off(MODAL_ON_SHOW_EVENT)
        $(`#${this.id}`).off(MODAL_ON_SHOWN_EVENT)
        $(`#${this.id}`).off(MODAL_ON_HIDE_EVENT)
        $(`#${this.id}`).off(MODAL_ON_HIDDEN_EVENT)

        $(`#${this.id}`).on(MODAL_ON_SHOW_EVENT, this.props.onShow)
        $(`#${this.id}`).on(MODAL_ON_SHOWN_EVENT, this.props.onShown)
        $(`#${this.id}`).on(MODAL_ON_HIDE_EVENT, this.props.onHide)
        $(`#${this.id}`).on(MODAL_ON_HIDDEN_EVENT, this.props.onHidden)
    }

    componentWillUpdate(nextProps) {
        if (this.props.toggle !== nextProps.toggle) {
            this.toggle()
        }
    }

    componentDidUpdate(previousProps) {
        if (this.props.onHide !== previousProps.onHide || this.props.onShow !== previousProps.onShow) {
            this.subscribeLifeCycle()
        }
    }

    render() {
        const { title, buttons, onConfirmed } = this.props

        return (
            <div className="modal fade" id={this.id} tabindex="-1" role="dialog" aria-labelledby={`${this.id}-title`} aria-hidden="true">
                <div className="modal-dialog modal-dialog-centered" style={this.props.style} role="document">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title">{title || "Title"}</h5>
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div className="modal-body">
                            {this.props.children}
                        </div>
                        <div className="modal-footer">
                            <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
                            {buttons}
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

ModalDialog.propTypes = {
    "id": PropTypes.string,
    "title": PropTypes.string,
    "footer": PropTypes.children,
    "show": PropTypes.bool,
    "onShow": PropTypes.func,
    "onShown": PropTypes.func,
    "onHide": PropTypes.func,
    "onHidden": PropTypes.func,
    "onConfirmed": PropTypes.func
}

export default ModalDialog
