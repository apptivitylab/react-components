/*
 * NetworkAlert.js
 * app/components
 *
 * Created by Li Theen Kok on 10 May 2017.
 * Copyright (C) 2017 Apptivity Lab. All Rights Reserved.
 */

import React, { Component } from "react"
import PropTypes from "prop-types"
import Alert from "./Alert"
import { animateScroll } from "react-scroll"

class NetworkAlert extends Component {
    hasAlerts() {
        if (!this.props.network) {
            return false
        }

        return this.props.network.pending.length > 0 || this.props.network.errors.length > 0
    }

    decodeHTML(htmlString) {
        const textareaDOMElement = document.createElement("textarea")
        textareaDOMElement.innerHTML = htmlString
        return textareaDOMElement.value
    }

    getErrorMessage(error) {
        if (error.response.data) {
            return typeof error.response.data.error === "object"
                ? this.decodeHTML(error.response.data.error.message)
                : error.response.data.error
        }
        return error.response.statusText
    }

    render() {
        if (!this.hasAlerts()) {
            return <div></div>
        }

        return (
            <div className={this.props.className || "row"}>
                {
                    this.props.network.pending.length > 0
                        ? <Alert alertType="info">
                    In progress: {this.props.network.pending.length} more to go...
                        </Alert>
                        : null
                }
                {
                    this.props.network.errors.length > 0
                        ? <Alert alertType="danger">
                            <ul>
                                {
                                    this.props.network.errors
                                        .map((error, index) => {
                                            if (error.response) {
                                                return <li key={index}>{`Error ${error.response.status}: ${this.getErrorMessage(error)}`}</li>
                                            }
                                            return <li key={index}>{error.toString()}</li>
                                        })
                                }
                            </ul>
                            {
                                this.props.onClearErrors
                                    ? <button type="button" className="btn btn-danger"
                                        onClick={(event) => {
                                            event.preventDefault()
                                            if (this.props.onClearErrors) {
                                                this.props.onClearErrors(this.props.network.errors)
                                            }
                                        }
                                        }
                                    >Clear</button>
                                    : null
                            }
                        </Alert>
                        : null
                }
                {
                    this.props.shouldScrollToTop && (this.props.network.pending.length > 0 || this.props.network.errors.length > 0) && animateScroll.scrollToTop()
                }
            </div>
        )
    }
}

NetworkAlert.defaultProps = {
    "shouldScrollToTop": true
}

NetworkAlert.propTypes = {
    "network": PropTypes.object.isRequired,
    "className": PropTypes.string,
    "onClearErrors": PropTypes.func,
    "shouldScrollToTop": PropTypes.bool
}

export default NetworkAlert
