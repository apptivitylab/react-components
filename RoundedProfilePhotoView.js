/*
 * RoundedProfilePhotoView.js
 * src/components
 *
 * Created by Li Theen Kok on 29 Jun 2017.
 * Copyright (C) 2017 Apptivity Lab. All Rights Reserved.
 */

import React, { Component } from "react"
import PropTypes from "prop-types"

class RoundedProfilePhotoView extends Component {
    constructor(props) {
        super(props)

        this.state = {
            "backgroundColor": props.backgroundColor || "#e0e0e0",
            "size": props.size || { "width": "100", "height": "100" },
            "name": props.name || "",
            "photoPath": props.photoPath || null
        }
    }

    get containerDisplayProperties() {
        return Object.assign({}, {
            "width": this.state.size.width,
            "height": this.state.size.height,
            "backgroundColor": this.state.backgroundColor
        })
    }

    get imageDisplayProperties() {
        return Object.assign({}, {
            "position": "absolute",
            "width": this.state.size.width + "px",
            "height": this.state.size.height + "px",
            "zIndex": 10
        })
    }

    get nameInitialsDisplayProperties() {
        return Object.assign({}, {
            "position": "absolute",
            "width": this.state.size.width,
            "height": this.state.size.height,
            "lineHeight": this.state.size.height + "px",
            "fontSize": 0.5 * parseInt(this.state.size.height) + "px",
            "fontWeight": "100",
            "color": "#ffffff"
        })
    }

    get nameInitials() {
        const nameComponents = this.state.name.split(" ")
        const initials = nameComponents.map((component) => component.substr(0, 1))
        if (initials.length > 1) {
            return [initials[0], initials[initials.length - 1]].join("")
        }
        return initials.join("")
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            "backgroundColor": nextProps.backgroundColor || "#e0e0e0",
            "size": nextProps.size || { "width": "100", "height": "100" },
            "name": nextProps.name || "",
            "photoPath": nextProps.photoPath || null
        })
    }

    render() {
        return (
            <div className={`rounded-circle ${this.props.className || ""}`} style={this.containerDisplayProperties}>
                {
                    this.state.photoPath
                        ? <img className="rounded-circle" style={this.imageDisplayProperties} src={this.state.photoPath} /> : null
                }
                <span className="text-center" style={this.nameInitialsDisplayProperties}>{this.nameInitials}</span>
            </div>
        )
    }
}

RoundedProfilePhotoView.propTypes = {
    "className": PropTypes.string,
    "backgroundColor": PropTypes.string,
    "size": PropTypes.object,
    "name": PropTypes.string,
    "photoPath": PropTypes.string
}

export default RoundedProfilePhotoView
