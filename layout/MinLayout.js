import React, { Component } from "react"
import PropTypes from "prop-types"

class MinLayout extends Component {
    render() {
        let style = ""
        if (this.props.minLayoutStyles) {
            style = this.props.minLayoutStyles.join(" ")
        }

        return (
            <div className={`${style}`}>
                {this.props.children}
            </div>
        )
    }
}

MinLayout.propTypes = {
    "minLayoutStyles": PropTypes.array
}

export default MinLayout
