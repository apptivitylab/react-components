import React, { Component } from "react"

import PropTypes from "prop-types"
import NavBar from "../navbar"

class NavBarLayout extends Component {
    render() {
        return (
            <div className="container-fluid w-100 p-0">
                <NavBar {...this.props}>{this.props.navBarItems}</NavBar>
                <main>{this.props.children}</main>
            </div>
        )
    }
}

NavBarLayout.propTypes = {
    "brandLogo": PropTypes.string,
    "brandName": PropTypes.string,
    "brandNameStyle": PropTypes.object,
    "brandNameClassName": PropTypes.string,
    "navBarItems": PropTypes.array,
    "navBarItemRight": PropTypes.object,
    "navBarStyles": PropTypes.array
}

export default NavBarLayout
