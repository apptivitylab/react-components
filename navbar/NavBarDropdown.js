import React, { Component } from "react"
import PropTypes from "prop-types"

class NavBarDropdown extends Component {
    render() {
        return (
            <div className="dropdown navbar-nav">
                <a className={`nav-item nav-link dropdown-toggle ${this.props.className}`} href="/" id={this.props.dropdownId} data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{this.props.title}</a>
                <div className="dropdown-menu dropdown-menu-right" aria-labelledby={this.props.dropdownId}>
                    {this.props.children}
                </div>
            </div>
        )
    }
}

NavBarDropdown.propTypes = {
    "dropdownId": PropTypes.string.isRequired,
    "className": PropTypes.string
}

export default NavBarDropdown
