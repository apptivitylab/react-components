import React, { Component } from "react"

class ProfileDropdown extends Component {
    render() {
        return (
            <div className="ml-auto dropdown navbar-nav">
                <a className="nav-item nav-link dropdown-toggle" href="/" id="profileMenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{this.props.title}</a>
                <div className="dropdown-menu dropdown-menu-right" aria-labelledby="profileMenu">
                    {this.props.children}
                </div>
            </div>
        )
    }
}

export default ProfileDropdown
