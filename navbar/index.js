import React, { Component } from "react"
import PropTypes from "prop-types"
import { Headline } from "../typography"

class NavBar extends Component {
    render() {
        return (
            <nav className={`navbar ${this.props.className}`}>
                <button className="navbar-toggler" type="button"
                    data-toggle="collapse"
                    data-target="#navbarToggleExternalContent"
                    aria-controls="navbarToggleExternalContent"
                    aria-expanded="false"
                    aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <a className="navbar-brand d-flex align-items-center" href="#">
                    <img style={{ "height": "1.5rem" }} className="mr-2" src={this.props.brandLogo} alt=""/>
                    <Headline className={this.props.brandNameClassName} style={this.props.brandNameStyle}>{this.props.brandName}</Headline>
                </a>

                <div className="collapse navbar-collapse" id="navbarToggleExternalContent">
                    <div className="navbar-nav mr-auto page-buttons">{this.props.children}</div>
                    {this.props.navBarItemRight}
                </div>
            </nav>
        )
    }
}

NavBar.propTypes = {
    "className": PropTypes.string,
    "brandLogo": PropTypes.string,
    "brandName": PropTypes.string,
    "navBarItemRight": PropTypes.object,
    "brandNameStyle": PropTypes.object,
    "brandNameClassName": PropTypes.string
}

export default NavBar
