import React, { Component } from "react"

class TableRow extends Component {
    render() {
        const { index, columns, actions, onActionClick } = this.props

        return (
            <tr key={index}>
                <th scope="row">{index + 1}</th>
                {
                    this.props.columns.map((item, columnIndex) => <td key={columnIndex}>{item}</td>)
                }

                <td key={index + "_actions"}>
                    {
                        this.props.actions.map(
                            (action, actionIndex) =>
                                <button key={action + "_button"} type="button" className="btn btn-primary mr-1" onClick={() => onActionClick(action)}>{action}</button>
                        )
                    }
                </td>
            </tr>
        )
    }
}

export default TableRow
