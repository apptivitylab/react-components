import React, { Component } from "react"

class Table extends Component {
    render() {
        const { tableStyles, headers, emptyDatasetMessage } = this.props

        if (!this.props.children || this.props.children.length === 0) {
            return (<span className="pl-4">{emptyDatasetMessage}</span>)
        } else {
            return (
                <table className={`table ${this.props.tableStyles.join(" ")}`}>
                    <thead>
                        <tr>
                            {this.props.headers.map((text, index) =>
                                <th key={index + text}>{text}</th>
                            )}
                        </tr>
                    </thead>
                    <tbody>
                        {this.props.children}
                    </tbody>
                </table>
            )
        }
    }
}

export default Table
