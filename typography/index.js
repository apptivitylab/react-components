import React, { Component } from "react"
import PropTypes from "prop-types"

class Title1 extends Component {
    render() {
        return (
            <h1 className={`title1 ${this.props.className || ""}`} style={this.props.style}>
                {this.props.content}{this.props.children}
            </h1>
        )
    }
}

class Title2 extends Component {
    render() {
        return (
            <h2 className={`title2 ${this.props.className || ""}`} style={this.props.style}>
                {this.props.content}{this.props.children}
            </h2>
        )
    }
}

class Title3 extends Component {
    render() {
        return (
            <h3 className={`title3 ${this.props.className || ""}`} style={this.props.style}>
                {this.props.content}{this.props.children}
            </h3>
        )
    }
}

class Headline extends Component {
    render() {
        return (
            <strong className={`headline ${this.props.className || ""}`} style={this.props.style}>
                {this.props.content}{this.props.children}
            </strong>
        )
    }
}

class Body extends Component {
    render() {
        return (
            <span className={`body ${this.props.className || ""}`} style={this.props.style}>
                {this.props.content}{this.props.children}
            </span>
        )
    }
}

class Callout extends Component {
    render() {
        return <span className={`callout ${this.props.className || ""}`} style={this.props.style}>{this.props.content}{this.props.children}</span>
    }
}

class Subhead extends Component {
    render() {
        return <h5 className={`subhead ${this.props.className || ""}`} style={this.props.style}>{this.props.content}{this.props.children}</h5>
    }
}

class Footnote extends Component {
    render() {
        return <footer className={`footnote ${this.props.className || ""}`} style={this.props.style}>{this.props.content}{this.props.children}</footer>
    }
}

class Caption1 extends Component {
    render() {
        return <small className={`caption1 ${this.props.className || ""}`} style={this.props.style}>{this.props.content}{this.props.children}</small>
    }
}

class Caption2 extends Component {
    render() {
        return <small className={`caption2 ${this.props.className || ""}`} style={this.props.style}>{this.props.content}{this.props.children}</small>
    }
}

export {
    Title1,
    Title2,
    Title3,
    Headline,
    Body,
    Callout,
    Subhead,
    Footnote,
    Caption1,
    Caption2
}
